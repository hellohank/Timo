package com.linln.modules.system.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class BaseService {
    @PersistenceContext
    protected EntityManager entityManager;
}
