-- 导出  表 timo.sys_user 结构
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE IF NOT EXISTS `sys_user` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` VARCHAR(255) DEFAULT NULL COMMENT '用户名',
  `nickname` VARCHAR(255) DEFAULT NULL COMMENT '用户昵称',
  `password` CHAR(64) DEFAULT NULL COMMENT '密码',
  `salt` VARCHAR(255) DEFAULT NULL COMMENT '密码盐',
  `dept_id` BIGINT(20) DEFAULT NULL COMMENT '部门ID',
  `picture` VARCHAR(255) DEFAULT NULL COMMENT '头像',
  `sex` TINYINT(4) DEFAULT NULL COMMENT '性别（1:男,2:女）',
  `email` VARCHAR(255) DEFAULT NULL COMMENT '邮箱',
  `phone` VARCHAR(255) DEFAULT NULL COMMENT '电话号码',
  `remark` VARCHAR(255) DEFAULT NULL COMMENT '备注',
  `createDate` DATETIME DEFAULT NULL COMMENT '创建时间',
  `updateDate` DATETIME DEFAULT NULL COMMENT '更新时间',
  `status` TINYINT(4) DEFAULT NULL COMMENT '状态（1:正常,2:冻结,3:删除）',
  PRIMARY KEY (`id`),
  KEY `FKb3pkx0wbo6o8i8lj0gxr37v1n` (`dept_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

-- 导出  表 timo.sys_dept 结构
DROP TABLE IF EXISTS sys_dept;
CREATE TABLE IF NOT EXISTS `sys_dept` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` VARCHAR(255) DEFAULT NULL COMMENT '部门名称',
  `pid` BIGINT(20) DEFAULT NULL COMMENT '父级ID',
  `pids` VARCHAR(255) DEFAULT NULL COMMENT '所有父级编号',
  `sort` INT(11) DEFAULT NULL COMMENT '排序',
  `remark` VARCHAR(255) DEFAULT NULL COMMENT '备注',
  `createDate` DATETIME DEFAULT NULL COMMENT '创建时间',
  `updateDate` DATETIME DEFAULT NULL COMMENT '更新时间',
  `create_by` BIGINT(20) DEFAULT NULL COMMENT '创建用户',
  `update_by` BIGINT(20) DEFAULT NULL COMMENT '更新用户',
  `status` TINYINT(4) DEFAULT NULL COMMENT '状态（1:正常,2:冻结,3:删除）',
  PRIMARY KEY (`id`),
  KEY `FKifwd1h4ciusl3nnxrpfpv316u` (`create_by`),
  KEY `FK83g45s1cjqqfpifhulqhv807m` (`update_by`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

-- 导出  表 timo.sys_action_log 结构
DROP TABLE IF EXISTS sys_action_log;
CREATE TABLE IF NOT EXISTS `sys_action_log` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` VARCHAR(255) DEFAULT NULL COMMENT '日志名称',
  `type` TINYINT(4) DEFAULT NULL COMMENT '日志类型',
  `ipaddr` VARCHAR(255) DEFAULT NULL COMMENT '操作IP地址',
  `clazz` VARCHAR(255) DEFAULT NULL COMMENT '产生日志的类',
  `method` VARCHAR(255) DEFAULT NULL COMMENT '产生日志的方法',
  `model` VARCHAR(255) DEFAULT NULL COMMENT '产生日志的表',
  `record_id` BIGINT(20) DEFAULT NULL COMMENT '产生日志的数据id',
  `message` TEXT COMMENT '日志消息',
  `createDate` DATETIME DEFAULT NULL COMMENT '记录时间',
  `oper_name` VARCHAR(255) DEFAULT NULL COMMENT '产生日志的用户昵称',
  `oper_by` BIGINT(20) DEFAULT NULL COMMENT '产生日志的用户',
  PRIMARY KEY (`id`),
  KEY `FK32gm4dja0jetx58r9dc2uljiu` (`oper_by`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;



-- 导出  表 timo.sys_dict 结构
DROP TABLE IF EXISTS sys_dict;
CREATE TABLE IF NOT EXISTS `sys_dict` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` VARCHAR(255) DEFAULT NULL COMMENT '字典名称',
  `name` VARCHAR(255) DEFAULT NULL COMMENT '字典键名',
  `type` TINYINT(4) DEFAULT NULL COMMENT '字典类型',
  `value` TEXT COMMENT '字典键值',
  `remark` VARCHAR(255) DEFAULT NULL COMMENT '备注',
  `createDate` DATETIME DEFAULT NULL COMMENT '创建时间',
  `updateDate` DATETIME DEFAULT NULL COMMENT '更新时间',
  `create_by` BIGINT(20) DEFAULT NULL COMMENT '创建用户',
  `update_by` BIGINT(20) DEFAULT NULL COMMENT '更新用户',
  `status` TINYINT(4) DEFAULT NULL COMMENT '状态（1:正常,2:冻结,3:删除）',
  PRIMARY KEY (`id`),
  KEY `FKag4shuprf2tjot9i1mhh37kk6` (`create_by`),
  KEY `FKoyng5jlifhsme0gc1lwiub0lr` (`update_by`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;


-- 导出  表 timo.sys_file 结构
DROP TABLE IF EXISTS sys_file;
CREATE TABLE IF NOT EXISTS `sys_file` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` VARCHAR(255) DEFAULT NULL COMMENT '文件名',
  `path` VARCHAR(255) DEFAULT NULL COMMENT '文件路径',
  `mime` VARCHAR(255) DEFAULT NULL COMMENT 'MIME文件类型',
  `size` BIGINT(20) DEFAULT NULL COMMENT '文件大小',
  `md5` VARCHAR(255) DEFAULT NULL COMMENT 'MD5值',
  `sha1` VARCHAR(255) DEFAULT NULL COMMENT 'SHA1值',
  `create_by` BIGINT(20) DEFAULT NULL COMMENT '上传者',
  `createDate` DATETIME DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `FKkkles8yp0a156p4247cc22ovn` (`create_by`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  timo.sys_file 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `sys_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_file` ENABLE KEYS */;

-- 导出  表 timo.sys_menu 结构
DROP TABLE IF EXISTS sys_menu;
CREATE TABLE IF NOT EXISTS `sys_menu` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` VARCHAR(255) DEFAULT NULL COMMENT '菜单名称',
  `pid` BIGINT(20) DEFAULT NULL COMMENT '父级编号',
  `pids` VARCHAR(255) DEFAULT NULL COMMENT '所有父级编号',
  `url` VARCHAR(255) DEFAULT NULL COMMENT 'URL地址',
  `perms` VARCHAR(255) DEFAULT NULL COMMENT '权限标识',
  `icon` VARCHAR(255) DEFAULT NULL COMMENT '图标',
  `type` TINYINT(4) DEFAULT NULL COMMENT '类型（1:一级菜单,2:子级菜单,3:不是菜单）',
  `sort` INT(11) DEFAULT NULL COMMENT '排序',
  `remark` VARCHAR(255) DEFAULT NULL COMMENT '备注',
  `createDate` DATETIME DEFAULT NULL COMMENT '创建时间',
  `updateDate` DATETIME DEFAULT NULL COMMENT '更新时间',
  `create_by` BIGINT(20) DEFAULT NULL COMMENT '创建用户',
  `update_by` BIGINT(20) DEFAULT NULL COMMENT '更新用户',
  `status` TINYINT(4) DEFAULT NULL COMMENT '状态（1:正常,2:冻结,3:删除）',
  PRIMARY KEY (`id`),
  KEY `FKoxg2hi96yr9pf2m0stjomr3we` (`create_by`),
  KEY `FKsiko0qcr8ddamvrxf1tk4opgc` (`update_by`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

-- 导出  表 timo.sys_role 结构
DROP TABLE IF EXISTS sys_role;
CREATE TABLE IF NOT EXISTS `sys_role` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` VARCHAR(255) DEFAULT NULL COMMENT '角色名称（中文名）',
  `name` VARCHAR(255) DEFAULT NULL COMMENT '标识名称',
  `remark` VARCHAR(255) DEFAULT NULL COMMENT '备注',
  `createDate` DATETIME DEFAULT NULL COMMENT '创建时间',
  `updateDate` DATETIME DEFAULT NULL COMMENT '更新时间',
  `create_by` BIGINT(20) DEFAULT NULL COMMENT '创建用户',
  `update_by` BIGINT(20) DEFAULT NULL COMMENT '更新用户',
  `status` TINYINT(4) DEFAULT NULL COMMENT '状态（1:正常,2:冻结,3:删除）',
  PRIMARY KEY (`id`),
  KEY `FKdkwvv0rm6j3d5l6hwsy2dplol` (`create_by`),
  KEY `FKrouqqi3f2bgc5o83wdstlh6q4` (`update_by`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;


-- 导出  表 timo.sys_role_menu 结构
DROP TABLE IF EXISTS sys_role_menu;
CREATE TABLE IF NOT EXISTS `sys_role_menu` (
  `role_id` BIGINT(20) NOT NULL,
  `menu_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`role_id`,`menu_id`),
  KEY `FKf3mud4qoc7ayew8nml4plkevo` (`menu_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;



-- 导出  表 timo.sys_user_role 结构
DROP TABLE IF EXISTS sys_user_role;
CREATE TABLE IF NOT EXISTS `sys_user_role` (
  `user_id` BIGINT(20) NOT NULL,
  `role_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKhh52n8vd4ny9ff4x9fb8v65qx` (`role_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;



-- 正在导出表  timo.sys_user 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `password`, `salt`, `dept_id`, `picture`, `sex`, `email`, `phone`, `remark`, `createDate`, `updateDate`, `status`) VALUES
	(1, 'admin', '超级管理员', '5fa04c0758ae596e2a93cd2802640693a5b08bdfacd4307abce323c85e481154', '7rl2t9', 1, NULL, 1, '10086@163.com', '10086', '', '2018-08-09 23:00:03', '2019-11-06 20:09:17', 1);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;

-- 正在导出表  timo.sys_dept 的数据：~5 rows (大约)
/*!40000 ALTER TABLE `sys_dept` DISABLE KEYS */;
INSERT INTO `sys_dept` (`id`, `title`, `pid`, `pids`, `sort`, `remark`, `createDate`, `updateDate`, `create_by`, `update_by`, `status`) VALUES
	(1, '总公司', 0, '[0]', 1, '', '2018-12-02 17:41:23', '2019-02-23 02:41:28', 1, 1, 1),
	(2, '产研部', 1, '[0],[1]', 1, '', '2018-12-02 17:51:04', '2019-04-27 13:12:46', 1, 1, 1);
/*!40000 ALTER TABLE `sys_dept` ENABLE KEYS */;



-- 正在导出表  timo.sys_role 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` (`id`, `title`, `name`, `remark`, `createDate`, `updateDate`, `create_by`, `update_by`, `status`) VALUES
	(1, '管理员', 'admin', '', '2018-09-29 00:12:40', '2019-01-18 21:09:51', 1, 1, 1);
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;

-- 正在导出表  timo.sys_user_role 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;


-- 正在导出表  timo.sys_dict 的数据：~6 rows (大约)
/*!40000 ALTER TABLE `sys_dict` DISABLE KEYS */;
INSERT INTO `sys_dict` (`id`, `title`, `name`, `type`, `value`, `remark`, `createDate`, `updateDate`, `create_by`, `update_by`, `status`) VALUES
	(1, '数据状态', 'DATA_STATUS', 2, '1:正常,2:冻结,3:删除', '', '2018-10-05 16:03:11', '2018-10-05 16:11:41', 1, 1, 1),
	(2, '字典类型', 'DICT_TYPE', 2, '2:键值对', '', '2018-10-05 20:08:55', '2019-01-17 23:39:23', 1, 1, 1),
	(3, '用户性别', 'USER_SEX', 2, '1:男,2:女', '', '2018-10-05 20:12:32', '2018-10-05 20:12:32', 1, 1, 1),
	(4, '菜单类型', 'MENU_TYPE', 2, '1:目录,2:菜单,3:按钮', '', '2018-10-05 20:24:57', '2019-11-06 20:08:46', 1, 1, 1),
	(5, '搜索栏状态', 'SEARCH_STATUS', 2, '1:正常,2:冻结', '', '2018-10-05 20:25:45', '2019-02-26 00:34:34', 1, 1, 1),
	(6, '日志类型', 'LOG_TYPE', 2, '1:业务,2:登录,3:系统', '', '2018-10-05 20:28:47', '2019-02-26 00:31:43', 1, 1, 1);
/*!40000 ALTER TABLE `sys_dict` ENABLE KEYS */;


-- 正在导出表  timo.sys_menu 的数据：~37 rows (大约)
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` (`id`, `title`, `pid`, `pids`, `url`, `perms`, `icon`, `type`, `sort`, `remark`, `createDate`, `updateDate`, `create_by`, `update_by`, `status`) VALUES
	(1, '菜单管理', 2, '[0],[2]', '/system/menu/index', 'system:menu:index', '', 2, 3, '', '2018-09-29 00:02:10', '2019-02-24 16:10:40', 1, 1, 1),
	(2, '系统管理', 0, '[0]', '#', '#', 'fa fa-cog', 1, 2, '', '2018-09-29 00:05:50', '2019-02-27 21:34:56', 1, 1, 1),
	(3, '添加', 1, '[0],[2],[1]', '/system/menu/add', 'system:menu:add', '', 3, 1, '', '2018-09-29 00:06:57', '2019-02-24 16:12:59', 1, 1, 1),
	(4, '角色管理', 2, '[0],[2]', '/system/role/index', 'system:role:index', '', 2, 2, '', '2018-09-29 00:08:14', '2019-02-24 16:10:34', 1, 1, 1),
	(5, '添加', 4, '[0],[2],[4]', '/system/role/add', 'system:role:add', '', 3, 1, '', '2018-09-29 00:09:04', '2019-02-24 16:12:04', 1, 1, 1),
	(6, '主页', 0, '[0]', '/index', 'index', 'layui-icon layui-icon-home', 1, 1, '', '2018-09-29 00:09:56', '2019-02-27 21:34:56', 1, 1, 1),
	(7, '用户管理', 2, '[0],[2]', '/system/user/index', 'system:user:index', '', 2, 1, '', '2018-09-29 00:43:50', '2019-04-05 17:43:25', 1, 2, 1),
	(8, '编辑', 1, '[0],[2],[1]', '/system/menu/edit', 'system:menu:edit', '', 3, 2, '', '2018-09-29 00:57:33', '2019-02-24 16:13:05', 1, 1, 1),
	(9, '详细', 1, '[0],[2],[1]', '/system/menu/detail', 'system:menu:detail', '', 3, 3, '', '2018-09-29 01:03:00', '2019-02-24 16:13:12', 1, 1, 1),
	(10, '修改状态', 1, '[0],[2],[1]', '/system/menu/status', 'system:menu:status', '', 3, 4, '', '2018-09-29 01:03:43', '2019-02-24 16:13:21', 1, 1, 1),
	(11, '编辑', 4, '[0],[2],[4]', '/system/role/edit', 'system:role:edit', '', 3, 2, '', '2018-09-29 01:06:13', '2019-02-24 16:12:10', 1, 1, 1),
	(12, '授权', 4, '[0],[2],[4]', '/system/role/auth', 'system:role:auth', '', 3, 3, '', '2018-09-29 01:06:57', '2019-02-24 16:12:17', 1, 1, 1),
	(13, '详细', 4, '[0],[2],[4]', '/system/role/detail', 'system:role:detail', '', 3, 4, '', '2018-09-29 01:08:00', '2019-02-24 16:12:24', 1, 1, 1),
	(14, '修改状态', 4, '[0],[2],[4]', '/system/role/status', 'system:role:status', '', 3, 5, '', '2018-09-29 01:08:22', '2019-02-24 16:12:43', 1, 1, 1),
	(15, '编辑', 7, '[0],[2],[7]', '/system/user/edit', 'system:user:edit', '', 3, 2, '', '2018-09-29 21:17:17', '2019-02-24 16:11:03', 1, 1, 1),
	(16, '添加', 7, '[0],[2],[7]', '/system/user/add', 'system:user:add', '', 3, 1, '', '2018-09-29 21:17:58', '2019-02-24 16:10:28', 1, 1, 1),
	(17, '修改密码', 7, '[0],[2],[7]', '/system/user/pwd', 'system:user:pwd', '', 3, 3, '', '2018-09-29 21:19:40', '2019-02-24 16:11:11', 1, 1, 1),
	(18, '权限分配', 7, '[0],[2],[7]', '/system/user/role', 'system:user:role', '', 3, 4, '', '2018-09-29 21:20:35', '2019-02-24 16:11:18', 1, 1, 1),
	(19, '详细', 7, '[0],[2],[7]', '/system/user/detail', 'system:user:detail', '', 3, 5, '', '2018-09-29 21:21:00', '2019-02-24 16:11:26', 1, 1, 1),
	(20, '修改状态', 7, '[0],[2],[7]', '/system/user/status', 'system:user:status', '', 3, 6, '', '2018-09-29 21:21:18', '2019-02-24 16:11:35', 1, 1, 1),
	(21, '字典管理', 2, '[0],[2]', '/system/dict/index', 'system:dict:index', '', 2, 5, '', '2018-10-05 00:55:52', '2019-02-24 16:14:24', 1, 1, 1),
	(22, '字典添加', 21, '[0],[2],[21]', '/system/dict/add', 'system:dict:add', '', 3, 1, '', '2018-10-05 00:56:26', '2019-02-24 16:14:10', 1, 1, 1),
	(23, '字典编辑', 21, '[0],[2],[21]', '/system/dict/edit', 'system:dict:edit', '', 3, 2, '', '2018-10-05 00:57:08', '2019-02-24 16:14:34', 1, 1, 1),
	(24, '字典详细', 21, '[0],[2],[21]', '/system/dict/detail', 'system:dict:detail', '', 3, 3, '', '2018-10-05 00:57:42', '2019-02-24 16:14:41', 1, 1, 1),
	(25, '修改状态', 21, '[0],[2],[21]', '/system/dict/status', 'system:dict:status', '', 3, 4, '', '2018-10-05 00:58:27', '2019-02-24 16:14:49', 1, 1, 1),
	(26, '行为日志', 2, '[0],[2]', '/system/actionLog/index', 'system:actionLog:index', '', 2, 6, '', '2018-10-14 16:52:01', '2019-02-27 21:34:15', 1, 1, 1),
	(27, '日志详细', 26, '[0],[2],[26]', '/system/actionLog/detail', 'system:actionLog:detail', '', 3, 1, '', '2018-10-14 21:07:11', '2019-02-27 21:34:15', 1, 1, 1),
	(28, '日志删除', 26, '[0],[2],[26]', '/system/actionLog/delete', 'system:actionLog:delete', '', 3, 2, '', '2018-10-14 21:08:17', '2019-02-27 21:34:15', 1, 1, 1),
	(30, '开发中心', 0, '[0]', '#', '#', 'fa fa-gavel', 1, 3, '', '2018-10-19 16:38:23', '2019-02-27 21:34:56', 1, 1, 1),
	(31, '代码生成', 30, '[0],[30]', '/dev/code', '#', '', 2, 1, '', '2018-10-19 16:39:04', '2019-03-13 17:43:58', 1, 1, 1),
	(125, '表单构建', 30, '[0],[30]', '/dev/build', '#', '', 2, 2, '', '2018-11-25 16:12:23', '2019-02-24 16:16:40', 1, 1, 1),
	(136, '部门管理', 2, '[0],[2]', '/system/dept/index', 'system:dept:index', '', 2, 4, '', '2018-12-02 16:33:23', '2019-02-24 16:10:50', 1, 1, 1),
	(137, '添加', 136, '[0],[2],[136]', '/system/dept/add', 'system:dept:add', '', 3, 1, '', '2018-12-02 16:33:23', '2019-02-24 16:13:34', 1, 1, 1),
	(138, '编辑', 136, '[0],[2],[136]', '/system/dept/edit', 'system:dept:edit', '', 3, 2, '', '2018-12-02 16:33:23', '2019-02-24 16:13:42', 1, 1, 1),
	(139, '详细', 136, '[0],[2],[136]', '/system/dept/detail', 'system:dept:detail', '', 3, 3, '', '2018-12-02 16:33:23', '2019-02-24 16:13:49', 1, 1, 1),
	(140, '改变状态', 136, '[0],[2],[136]', '/system/dept/status', 'system:dept:status', '', 3, 4, '', '2018-12-02 16:33:23', '2019-02-24 16:13:57', 1, 1, 1),
	(146, '数据接口', 30, '[0],[30]', '/dev/swagger', '#', '', 2, 3, '', '2018-12-09 21:11:11', '2019-02-24 23:38:18', 1, 1, 1);
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;



-- 正在导出表  timo.sys_role_menu 的数据：~69 rows (大约)
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES
	(1, 1),
	(1, 2),
	(1, 3),
	(1, 4),
	(1, 5),
	(1, 6),
	(1, 7),
	(1, 8),
	(1, 9),
	(1, 10),
	(1, 11),
	(1, 12),
	(1, 13),
	(1, 14),
	(1, 15),
	(1, 16),
	(1, 17),
	(1, 18),
	(1, 19),
	(1, 20),
	(1, 21),
	(1, 22),
	(1, 23),
	(1, 24),
	(1, 25),
	(1, 26),
	(1, 27),
	(1, 28),
	(1, 30),
	(1, 31),
	(1, 125),
	(1, 136),
	(1, 137),
	(1, 138),
	(1, 139),
	(1, 140),
	(1, 146);
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
DROP TABLE IF EXISTS sys_schedule;
CREATE TABLE `sys_schedule` (
                                `id` bigint NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                                `title` varchar(200) COLLATE utf8mb4_general_ci NOT NULL COMMENT '定时计划标题',
                                `remark` text COLLATE utf8mb4_general_ci COMMENT '详细备注说明',
                                `cron` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'cron定时表达式',
                                `execcode` text COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务要执行的代码（java1.4规范）',
                                `status` tinyint NOT NULL DEFAULT '1' COMMENT '状态：1-正常；2-冻结；3-删除',
                                PRIMARY KEY (`id`),
                                KEY `idx_sys_schedule_1` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS monitor_alarm;
CREATE TABLE `monitor_alarm` (
                                 `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
                                 `srv_type` tinyint NOT NULL DEFAULT '1' COMMENT '服务类型：1-dubbo应用、2-http服务、3-grpc服务',
                                 `sev_data_id` bigint NOT NULL COMMENT '服务对应的表主键id',
                                 `dimension` tinyint NOT NULL COMMENT '监控的维度：1-节点存活；2-线程数；3-内存；',
                                 `content` text COLLATE utf8mb4_general_ci COMMENT '预警的内容',
                                 `alarm_time` datetime NOT NULL COMMENT '上一次预警时间',
                                 `status` tinyint NOT NULL DEFAULT '1' COMMENT '状态：1-正常；2-冻结；3-删除',
                                 PRIMARY KEY (`id`),
                                 KEY `idx_monitor_alarm_1` (`srv_type`,`sev_data_id`),
                                 KEY `idx_monitor_alarm_2` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

insert into `sys_menu` (`id`, `title`, `pid`, `pids`, `url`, `perms`, `icon`, `type`, `sort`, `remark`,  `create_by`, `update_by`, `status`, `createDate`, `updateDate`) values('243','定时计划管理','2','[0],[2]','/system/schedule/index?status=1','system:schedule:index','','2','7','','1','2','1','2022-09-03 16:22:40','2022-09-05 21:04:38');
insert into `sys_menu` (`id`, `title`, `pid`, `pids`, `url`, `perms`, `icon`, `type`, `sort`, `remark`,  `create_by`, `update_by`, `status`, `createDate`, `updateDate`) values('244','添加','243','[0],[2],[243]','/system/schedule/add','system:schedule:add',NULL,'3','1',NULL,'1','1','1','2022-09-03 16:22:40','2022-09-03 16:22:40');
insert into `sys_menu` (`id`, `title`, `pid`, `pids`, `url`, `perms`, `icon`, `type`, `sort`, `remark`,  `create_by`, `update_by`, `status`, `createDate`, `updateDate`) values('245','编辑','243','[0],[2],[243]','/system/schedule/edit','system:schedule:edit',NULL,'3','2',NULL,'1','2','1','2022-09-03 16:22:40','2022-09-05 21:49:00');
insert into `sys_menu` (`id`, `title`, `pid`, `pids`, `url`, `perms`, `icon`, `type`, `sort`, `remark`,  `create_by`, `update_by`, `status`, `createDate`, `updateDate`) values('246','详细','243','[0],[2],[243]','/system/schedule/detail','system:schedule:detail',NULL,'3','3',NULL,'1','2','1','2022-09-03 16:22:40','2022-09-05 21:49:00');
insert into `sys_menu` (`id`, `title`, `pid`, `pids`, `url`, `perms`, `icon`, `type`, `sort`, `remark`,  `create_by`, `update_by`, `status`, `createDate`, `updateDate`) values('247','修改状态','243','[0],[2],[243]','/system/schedule/status','system:schedule:status',NULL,'3','4',NULL,'1','2','1','2022-09-03 16:22:40','2022-09-05 21:49:00');
insert into `sys_menu` (`id`, `title`, `pid`, `pids`, `url`, `perms`, `icon`, `type`, `sort`, `remark`,  `create_by`, `update_by`, `status`, `createDate`, `updateDate`) values('248','试运行脚本','243','[0],[2],[243]','/system/schedule/runscript','system:schedule:runscript','','3','5','','2','2','1','2022-09-05 21:49:00','2022-09-05 21:49:00');
