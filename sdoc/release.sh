#!/bin/bash
pwd
git clone -b xxxxx https://gitee.com/hellohank/Timo.git
cd Timo
mvn clean package -Dmaven.test.skip=true

cd ..
cp -f Timo/admin/target/timo-admin.jar ./timo-admin.jar

rm -rf Timo

APP_PORT=8080

# 关闭应用
(ps -ef | grep timo-admin.jar | grep -v grep | awk '{print $2}' | xargs kill -9)||(echo "")
nohup java -jar -Xms450m -Xmx450m timo-admin.jar --spring.profiles.active=prod --server.port=$APP_PORT --datasource.domain=172.18.12.36:3308 --spring.datasource.username=iyunuser --spring.datasource.password="IyUn9@6{)qq" > run.log 2>&1 &

STATUS=1
for i in `seq 1 120`;do
    code=`curl -I http://127.0.0.1:${APP_PORT}/index --connect-timeout 2|grep HTTP|awk '{print $2}' 2>/dev/null`
    if [[ ${code} -eq 200 ]];then
      STATUS=0
      echo "timo started successfully !"
      break
    else
      sleep 1
    fi
done

if [[ STATUS -eq 0 ]];then
    echo "启动成功"
else
    echo "启动失败"
    pwd
    cat run.log
    exit -127
fi