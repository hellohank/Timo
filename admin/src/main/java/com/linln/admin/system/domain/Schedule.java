package com.linln.admin.system.domain;

import com.linln.common.utils.StatusUtil;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author taofucheng
 * @date 2022/09/03
 */
@Data
@Entity
@Table(name="sys_schedule")
@EntityListeners(AuditingEntityListener.class)
@Where(clause = StatusUtil.NOT_DELETE)
@DynamicInsert
@DynamicUpdate
public class Schedule implements Serializable {
    // 自增主键
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    // 定时计划标题
    private String title;
    // 详细备注说明
    private String remark;
    // cron定时表达式
    private String cron;
    // 任务要执行的代码（java1.4规范）
    private String execcode;
    // 状态：1-正常；2-冻结；3-删除
    private Byte status;
}