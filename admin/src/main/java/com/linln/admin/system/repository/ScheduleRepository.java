package com.linln.admin.system.repository;

import com.linln.admin.system.domain.Schedule;
import com.linln.common.config.BaseRepository;

import java.util.List;

/**
 * @author taofucheng
 * @date 2022/09/03
 */
public interface ScheduleRepository extends BaseRepository<Schedule, Long> {
    List<Schedule> findByStatus(Byte status);
}