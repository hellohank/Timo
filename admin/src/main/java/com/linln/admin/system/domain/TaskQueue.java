package com.linln.admin.system.domain;

import com.linln.common.utils.StatusUtil;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * @author taofucheng
 * @date 2021/07/12
 */
@Data
@Entity
@Table(name="sys_task_queue")
@EntityListeners(AuditingEntityListener.class)
@Where(clause = StatusUtil.NOT_DELETE)
@DynamicInsert
@DynamicUpdate
public class TaskQueue implements Serializable {
    //
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    // 任务类型：1-新建发布分支；2-确认已成功上线；3-将指定项目开发分支代码合并到发布分支；4-代码发布前规范测试；……
    private String type;
    // 触发任务的对应的主键值，如：app_version.id
    private Long relate_id;
    // 创建时间
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date createDate;
    // 状态（1:待执行,2:已完成,3:操作失败）
    @Column(name = "`status`")
    private Integer status;
    // 执行结束后的提示信息，如果失败，则是报错提示或信息
    @Lob
    @Column(columnDefinition="TEXT")
    private String msg;

    @Lob
    @Column(columnDefinition = "TEXT")
    private String data;
}