package com.linln.admin.system.validator;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author taofucheng
 * @date 2022/09/03
 */
@Data
public class ScheduleValid implements Serializable {
    @NotEmpty(message = "定时计划标题不能为空")
    private String title;
    @NotEmpty(message = "cron定时表达式不能为空")
    private String cron;
    @NotEmpty(message = "任务要执行的代码（java1.4规范）不能为空")
    private String execcode;
}