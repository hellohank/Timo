package com.linln.admin.system.domain;

import com.linln.common.utils.StatusUtil;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author taofucheng
 * @date 2022/08/16
 */
@Data
@Entity
@Table(name="monitor_alarm")
@EntityListeners(AuditingEntityListener.class)
@Where(clause = StatusUtil.NOT_DELETE)
@DynamicInsert
@DynamicUpdate
public class Alarm implements Serializable {
    // 主键
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    // 服务类型：1-dubbo应用、2-http服务、3-grpc服务
    private Integer srv_type;
    // 服务对应的表主键id
    private Long sev_data_id;
    // 监控的维度：1-节点存活；2-线程数；3-内存；
    private Integer dimension;
    // 预警的内容
    private String content;
    // 上一次预警时间
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date alarm_time;
    // 状态：1-正常；2-冻结；3-删除
    private Byte status;
}