package com.linln.admin.system.service;

import com.linln.admin.system.domain.TaskQueue;
import com.linln.common.enums.StatusEnum;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author taofucheng
 * @date 2021/07/12
 */
public interface TaskQueueService {
    /**
     * 任务状态：待执行
     */
    public static final Integer TASK_STATUS_NEW = 1;
    /**
     * 任务状态：执行成功
     */
    public static final Integer TASK_STATUS_SUCC = 2;
    /**
     * 任务状态：执行失败
     */
    public static final Integer TASK_STATUS_FAIL = 3;
    /**
     * 获取分页列表数据
     *
     * @param example 查询实例
     * @return 返回分页数据
     */
    Page<TaskQueue> getPageList(Example<TaskQueue> example);

    /**
     * 根据ID查询数据
     *
     * @param id 主键ID
     */
    TaskQueue getById(Long id);

    /**
     * 获取指定数据下的一个可执行的任务的描述信息
     *
     * @param relatedId
     * @return
     */
    String getOneTodoDescriptByRelatedId(Long relatedId);

    /**
     * 保存数据
     *
     * @param taskQueue 实体对象
     */
    TaskQueue save(TaskQueue taskQueue);

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Transactional(rollbackFor = Exception.class)
    Boolean updateStatus(StatusEnum statusEnum, Long id);

    /**
     * 获取指定版本代码中最后一次代码规范检查的结果
     *
     * @param relatedId
     * @return
     */
    String getLastPmdCheckResultByRelatedId(long relatedId);

    /**
     * 清空指定检查任务中的信息
     *
     * @param relatedId
     * @return
     */
    boolean clearLastPmdCheckResultByRelatedId(long relatedId);
}