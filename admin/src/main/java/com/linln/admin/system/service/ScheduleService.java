package com.linln.admin.system.service;

import com.linln.admin.system.domain.Schedule;
import com.linln.common.enums.StatusEnum;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author taofucheng
 * @date 2022/09/03
 */
public interface ScheduleService {

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    Page<Schedule> getPageList(Example<Schedule> example);

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    Schedule getById(Long id);

    /**
     * 保存数据
     * @param schedule 实体对象
     */
    Schedule save(Schedule schedule);

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Transactional(rollbackFor = Exception.class)
    Boolean updateStatus(StatusEnum statusEnum, List<Long> idList);
}