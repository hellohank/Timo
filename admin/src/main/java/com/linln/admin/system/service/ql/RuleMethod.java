package com.linln.admin.system.service.ql;

import java.lang.annotation.*;

/**
 * 定义规则方法
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface RuleMethod {
    String description() default "";
}
