package com.linln.admin.system.repository;

import com.linln.admin.system.domain.TaskQueue;
import com.linln.common.config.BaseRepository;

/**
 * @author taofucheng
 * @date 2021/07/12
 */
public interface TaskQueueRepository extends BaseRepository<TaskQueue, Long> {

}