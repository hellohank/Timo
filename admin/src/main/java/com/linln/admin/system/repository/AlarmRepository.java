package com.linln.admin.system.repository;

import com.linln.admin.system.domain.Alarm;
import com.linln.common.config.BaseRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author taofucheng
 * @date 2022/08/16
 */
public interface AlarmRepository extends BaseRepository<Alarm, Long> {

    @Query("from #{#entityName} t where srv_type=:servType and sev_data_id=:servId and dimension=:dimension")
    Alarm findByServTypeAndId(int servType, Long servId, int dimension);
}