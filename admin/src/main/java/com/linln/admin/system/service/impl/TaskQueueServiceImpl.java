package com.linln.admin.system.service.impl;

import com.linln.admin.system.domain.TaskQueue;
import com.linln.admin.system.repository.TaskQueueRepository;
import com.linln.admin.system.service.TaskQueueService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author taofucheng
 * @date 2021/07/12
 */
@Service
public class TaskQueueServiceImpl implements TaskQueueService {

    @Autowired
    private TaskQueueRepository taskQueueRepository;

    /**
     * 根据ID查询数据
     *
     * @param id 主键ID
     */
    @Override
    public TaskQueue getById(Long id) {
        return taskQueueRepository.findById(id).orElse(null);
    }

    @Override
    public String getOneTodoDescriptByRelatedId(Long relatedId) {
        TaskQueue tq = new TaskQueue();
        tq.setRelate_id(relatedId);
        tq.setStatus(TASK_STATUS_NEW);
        List<TaskQueue> ret = taskQueueRepository.findAll(Example.of(tq));
        tq = ret == null || ret.isEmpty() ? null : ret.get(0);

        if (tq == null) {
            return "";
        }
        return tq.getMsg();
    }

    /**
     * 获取分页列表数据
     *
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<TaskQueue> getPageList(Example<TaskQueue> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return taskQueueRepository.findAll(example, page);
    }

    /**
     * 保存数据
     *
     * @param taskQueue 实体对象
     */
    @Override
    public TaskQueue save(TaskQueue taskQueue) {
        return taskQueueRepository.save(taskQueue);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateStatus(StatusEnum statusEnum, Long id) {
        List<Long> idList = new ArrayList<>();
        idList.add(id);
        return taskQueueRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }

    /**
     * 获取指定版本代码中最后一次代码规范检查的结果
     *
     * @param relatedId
     * @return
     */
    @Override
    public String getLastPmdCheckResultByRelatedId(long relatedId) {
        TaskQueue tq = getOneChechPmdTask(relatedId);
        if (tq == null) {
            return "";
        } else {
            return tq.getMsg();
        }
    }

    private TaskQueue getOneChechPmdTask(long relatedId) {
        TaskQueue tq = new TaskQueue();
        tq.setRelate_id(relatedId);
//        tq.setType(AppConstants.TASK_TYPE_CHECK_PMD);
        tq.setStatus(TASK_STATUS_SUCC);
        List<TaskQueue> ret = taskQueueRepository.findAll(Example.of(tq), Sort.by("id").descending());
        tq = ret == null || ret.isEmpty() ? null : ret.get(0);
        return tq;
    }

    /**
     * 清空指定检查任务中的信息
     *
     * @param relatedId
     * @return
     */
    @Override
    public boolean clearLastPmdCheckResultByRelatedId(long relatedId) {
        TaskQueue tq = getOneChechPmdTask(relatedId);
        if (tq != null) {
            tq.setMsg("");
            taskQueueRepository.save(tq);
        }
        return true;
    }
}