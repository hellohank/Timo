package com.linln.admin.system.controller;

import com.linln.admin.system.domain.TaskQueue;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.vo.ResultVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author taofucheng
 * @date 2021/07/12
 */
@Controller
@RequestMapping("/system/taskQueue")
public class TaskQueueController {

    /**
     * 查询指定任务的状态信息
     */
    @GetMapping("/status/{id}")
    @ResponseBody
    public ResultVo toDetail(@PathVariable("id") TaskQueue taskQueue, Model model) {
        if (taskQueue == null) {
            return ResultVoUtil.error("没有该任务信息！");
        }
        return ResultVoUtil.success(taskQueue.getMsg(), taskQueue.getId());
    }

}