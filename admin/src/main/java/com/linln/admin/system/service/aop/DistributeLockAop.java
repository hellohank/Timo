package com.linln.admin.system.service.aop;

import com.linln.admin.system.service.DistributeLockComponent;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;

@Aspect
@Component
public class DistributeLockAop {
    @Resource
    private DistributeLockComponent distributeLockComponent;

    @Around("@annotation(com.linln.admin.system.service.aop.DistributeLock)")
    public Object distributeLock(ProceedingJoinPoint point) throws Throwable {
        System.out.println("进入aop。。。");
        Method targetMethod = ((MethodSignature) (point.getSignature())).getMethod();
        DistributeLock lock = targetMethod.getAnnotation(DistributeLock.class);
        String key = lock.key();
        if(!distributeLockComponent.tryLock(key,60)){
            //已经有操作在进行了
            throw new RuntimeException();
        }
        return point.proceed();
    }
}
