package com.linln.devtools.generate.domain;

import com.linln.common.utils.ToolUtil;

import java.util.List;

/**
 * @author taofucheng
 * @date 2018/10/21
 */
public class Field {
    private String name;
    private String title;
    private Integer type;
    private Integer query;
    private boolean show;
    private List<Integer> verify;

    public Field() {
    }

    public Field(String name, String title, int type, int query, boolean show, List<Integer> verify) {
        this.name = name;
        this.title = title;
        this.type = type;
        this.query = query;
        this.show = show;
        this.verify = verify;
    }

    /**
     * 返回驼峰形式的名字
     *
     * @return
     */
    public String getName() {
        return ToolUtil.fieldToProperty(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRawName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getQuery() {
        return query;
    }

    public void setQuery(Integer query) {
        this.query = query;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public List<Integer> getVerify() {
        return verify;
    }

    public void setVerify(List<Integer> verify) {
        this.verify = verify;
    }
}
