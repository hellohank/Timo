##Service层模板

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.linln.modules.system.service.BaseService;

import java.util.List;

/**
 * @author taofucheng
 * @date 2019/4/4
 */
@Service
@Slf4j
public class #{entity}ServiceImpl extends BaseService implements #{entity}Service {

    @Autowired
    private #{entity}Repository #{name}Repository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    public #{entity} getById(Long id) {
        return #{name}Repository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<#{entity}> getPageList(Example<#{entity}> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return #{name}Repository.findAll(example, page);
    }

    @Override
        public Page<#{entity}> getPageList(Example<#{entity}> example, int pageIndex, int pageSize) {
            // 创建分页对象
            PageRequest page = PageSort.pageRequest(pageIndex, pageSize);
            return #{name}Repository.findAll(example, page);
        }

    /**
     * 保存数据
     * @param #{name} 实体对象
     */
    @Override
    public #{entity} save(#{entity} #{name}){
        if(#{name}.getId()==null){//如果是新增的，则填充默认值
            ShiroUtil.setDefaultValue(#{name});
        }
        return #{name}Repository.save(#{name});
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList){
        return #{name}Repository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}