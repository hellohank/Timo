##dao层模板

/**
 * @author taofucheng
 * @date 2019/4/4
 */
public interface #{entity}Repository extends BaseRepository<#{entity}, Long> {
}