package com.linln.component.excel.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author taofucheng <auntvt＠163.com>
 * @date 2021/3/19
 */
@ComponentScan(basePackages = "com.linln.component.excel")
public class ExcelAutoConfig {
}
