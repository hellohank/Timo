package com.linln.component.thymeleaf;

import com.linln.component.thymeleaf.utility.DictUtil;
import com.linln.component.thymeleaf.utility.EntityUtils;
import com.linln.component.thymeleaf.utility.LogUtil;
import com.linln.component.thymeleaf.utility.PageUtil;
import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.expression.IExpressionObjectFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author taofucheng
 * @date 2018/8/14
 */
public class TimoExpressionObjectFactory implements IExpressionObjectFactory {

    private static final Map<String, Object> utilities = new HashMap<>();

    static {
        utilities.put("pageUtil", new PageUtil());
        utilities.put("dicts", new DictUtil());
        utilities.put("logs", new LogUtil());
        utilities.put("entity", new EntityUtils());
    }

    @Override
    public Set<String> getAllExpressionObjectNames() {
        Set<String> names = Collections.unmodifiableSet(utilities.keySet());
        return names;
    }

    @Override
    public Object buildObject(IExpressionContext context, String expressionObjectName) {
        return utilities.get(expressionObjectName);
    }

    @Override
    public boolean isCacheable(String expressionObjectName) {
        return expressionObjectName != null;
    }
}
