package com.linln.component.thymeleaf.utility;

import com.linln.common.utils.ReflectUtils;

import java.util.Collection;
import java.util.Map;

public class EntityUtils {
    public Object readField(Object entity, Object fieldName) {
        if (entity instanceof Map) {
            return ((Map) entity).get(fieldName);
        } else {
            return ReflectUtils.readField(entity, fieldName+"");
        }
    }

    public boolean equals(Object one, Object another) {
        if (one == null && another == null) {
            return true;
        }
        if (one == null) {
            return false;
        }
        if (another instanceof Collection && ((Collection) another).size() > 0) {
            another = ((Collection) another).iterator().next();
        }
        return String.valueOf(one).equals(String.valueOf(another));
    }
}
